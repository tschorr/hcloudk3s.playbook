# Ansible Playbook to deploy K3s on Hetzner Cloud

Yet another Ansible playbook to deploy a k3s cluster in a Hetzner Cloud project, on a private network behind a firewall.
Will also install the Hetzner Cloud Controller Manager, the K3S System Upgrade Controller and Longhorn.
Network, firewall, VM creation and K3S installation follow [this blog post](https://community.hetzner.com/tutorials/k3s-glusterfs-loadbalancer).

## Features

* Debian as the base OS.
* Disable SSH root login and provide non root user for all operations after VM creation.
* K3s is installed on a private network, protected by a Hetzner firewall.
* Install Longhorn as the distributed storage system.
* Install the Hetzner Cloud Controller Manager.
* Install k3s System Upgrade Controller.

## Prerequisites

* A Hetzner Cloud project with an API token.
* An SSH pubkey for the user executing the playbook needed for provisioning the VMs.
* The SSH pubkey for the user you wish to be able to login to the cluster VMs once provisioning is complete.

## Usage

```
$ ANSIBLE_HOST_KEY_CHECKING=false K3STOKEN="<your k3s token>" USERNAME="<linux username>" PASSWORD="<password hash (see notes below)>" PUBKEY="<linux user SSH pubkey>" HETZNER_API_TOKEN="<your API token>" ansible-playbook site.yml -K -i inventories/default.yml
```

### Default Inventory

There is a default inventory file is `inventories/default.yml`.
Currently you can configure the SSH pubkey name needed for VM provisioning, the Hetzner server type (defaults to cx11) and the number of controlplane and agent nodes.

### Notes:

* The PASSWORD needs to be encrypted following the [ansible.builtin.user module documentation](https://docs.ansible.com/ansible/latest/collections/ansible/builtin/user_module.html#parameter-password) and the [FAQ](https://docs.ansible.com/ansible/latest/reference_appendices/faq.html#how-do-i-generate-encrypted-passwords-for-the-user-module).
* You need to escape special characters in the generated password hash, namely the "$" character (or use single quotes).
